# The Diamond Kata

A script that given a character from the alphabet, prints a diamond of its output with that character being the midpoint of the diamond.

## Prerequisites

In order to run this script, you need to have Node.js installed on your computer. The recommended version is 16. It was tested only on Node.js v16.15.0.

## How to run

- Clone this repository
- Open the repository directory with Terminal
- Run to install packages:
```console
foo@bar:~$ npm install
```
- Run to build:
```console
foo@bar:~$ npm run build
```

- Run to start the script:
```console
foo@bar:~$ npm start
```
- You can also run three above steps install at once:
```console
foo@bar:~$ npm installAndStart
```

- For running the tests, please use:
```console
foo@bar:~$ npm test
```

## Usage

After running the script you will be asked for providing one character and pressing Enter. The input character should be a latin letter. Otherwise, you will be informed that your input was incorrect and asked to try again.

If the characted you provided is correct, a 'diamond' will be printed in the console. After that you can pass use the script again by providing a new character, or end the script by pressing Crtl+C.

