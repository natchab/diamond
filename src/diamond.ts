import readline from 'readline';

import { generateDiamond, printLines, checkIfSingleLetter } from './helpers';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

rl.write('Please input a single latin letter and press Enter. Lowercase characters will be transformed to uppercase.\n');

rl.on('line', (input) => {
  if (!checkIfSingleLetter(input)) {
    console.log('Only single letters from latin alphabet are accepted. Please try again or quit by pressing Ctrl+C.\n');
    return;
  }

  const diamondLines = generateDiamond(input);
  printLines(diamondLines);

  console.log('Input a new character to generate a diamond. Press Ctrl+C to quit.\n');
});

rl.on('close', function () {
  console.log('\nThank you for using this script.');
  process.exit(0);
});