export const singleLetterRegEx = /^[a-zA-Z]$/;

export const checkIfSingleLetter = (input: string) => {
  return (singleLetterRegEx.test(input))
}

export const appendSpacesToArray = (arr: string[], numberOfCopies: number) => {
  const newArr = [...arr];
  for(let k = numberOfCopies; k > 0; k--) {
    newArr.push(' ');
  }
  return newArr;
}

export const generateDiamond = (str: string) => {
  const characterCode = str.toUpperCase().charCodeAt(0);
  const halfLineLength = characterCode - 65 + 1;
  const pyramidLines: string[] = [];
  for (let i = 0; i < characterCode - 65 + 1; i++) {
    let arr: string[] = [];
    arr = appendSpacesToArray(arr, characterCode - 65 - i);
    arr.push(String.fromCharCode(65 + i));
    arr = appendSpacesToArray(arr, halfLineLength - arr.length);
    pyramidLines.push(arr.concat([...arr].reverse().slice(1)).join(''));
  }
  return pyramidLines.concat([...pyramidLines].reverse().slice(1));
}

export const printLines = (lines: string[]) => {
  lines.forEach(line => console.log(line));
}