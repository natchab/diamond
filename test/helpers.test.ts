import {checkIfSingleLetter, appendSpacesToArray, generateDiamond, printLines} from '../src/helpers';

const logSpy = jest.spyOn(console, 'log');

describe("test checkIfSingleLetter function", () => {
  it("should return true for a single lower case character", () => {
    expect(checkIfSingleLetter('a')).toBe(true);
  });

  it("should return true for a single upper case character", () => {
    expect(checkIfSingleLetter('A')).toBe(true);
  });

  it("should return false for a single non-latin character", () => {
    expect(checkIfSingleLetter('Ć')).toBe(false);
  });

  it("should return false for a character which is not a letter", () => {
    expect(checkIfSingleLetter('7')).toBe(false);
  });

  it("should return false for multiple characters", () => {
    expect(checkIfSingleLetter('abc')).toBe(false);
  });
});

describe("test appendSpacesToArray function", () => {
  it("should return an array with two spaces", () => {
    const expectedArray = [' ', ' '];
    expect(appendSpacesToArray([], 2)).toEqual(expectedArray);
  });

  it("should return the original array with two spaces appended", () => {
    const originalArray = ['a'];
    const expectedArray = ['a', ' ', ' '];
    expect(appendSpacesToArray(originalArray, 2)).toEqual(expectedArray);
  });

  it("should return the original array with no spaces appended", () => {
    const originalArray = ['a'];
    const expectedArray = ['a'];
    expect(appendSpacesToArray(originalArray, 0)).toEqual(expectedArray);
  });
});

describe("test generateDiamond function", () => {
  it("should return a correct array of strings for 'A' character", () => {
    const expectedArray = ['A'];
    expect(generateDiamond('A')).toEqual(expectedArray);
  });

  it("should return a correct array of strings for the lower case 'a' character", () => {
    const expectedArray = ['A'];
    expect(generateDiamond('a')).toEqual(expectedArray);
  });

  it("should return a correct array of strings for a 'D' character", () => {
    const expectedArray = ['   A   ', '  B B  ', ' C   C ', 'D     D', ' C   C ','  B B  ', '   A   '];
    expect(generateDiamond('D')).toEqual(expectedArray);;
  });
});


describe("test printLines function", () => {
  it("should call console.log with correct strings", () => {
    const expectedArray = ['A'];
    printLines(['a', 'bc']);
    expect(logSpy).toHaveBeenCalledWith('a');
    expect(logSpy).toHaveBeenCalledWith('bc');
  });
});

